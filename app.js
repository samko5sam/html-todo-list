// 注意
// 未完成：儲存完成狀態

// var
var todos;

// selectors
const todoInput = document.querySelector(".todo-input");
const todoButton = document.querySelector(".todo-button");
const todoList = document.querySelector(".todo-list");
const filterOption = document.querySelector(".filter-todo");

// event listeners
document.addEventListener("DOMContentLoaded",getTodos)
todoButton.addEventListener("click",addTodo);
todoList.addEventListener("click", deleteCheck);
filterOption.addEventListener("click",filterTodo);

// functions
function addTodo(event) {
  // prevent submit
  event.preventDefault();

  createTodoListItem(todoInput.value,"y",);

  // clear text input
  todoInput.value = "";
}
function deleteCheck(e) {
  const item = e.target;
  const myclass = item.classList[0];
  const todo = item.parentElement;

  if (myclass === "trash-button"){
    // delete todo item
    todo.remove();
    removeLocalTodos(todo.children[0].innerText);
  }else if (myclass === "complete-button") {
    // complete
    todo.classList.toggle("completed");
  }
}
function filterTodo(e) {
  const todos = todoList.childNodes;
  todos.forEach(function(todo) {
    switch(e.target.value){
      case "all":
        todo.style.display = "flex";
        break;
      case "completed":
        if (todo.classList.contains("completed")) {
          todo.style.display = "flex";
        }else {
          todo.style.display = "none";
        }
        break;
      case "uncompleted":
        if (!todo.classList.contains("completed")) {
          todo.style.display = "flex";
        }else {
          todo.style.display = "none";
        }
        break;
    }
  });
}
function checkLocalStorage() {
  // check
  if (localStorage.getItem("todos") === null) {
    todos = [];
  }else {
    todos = JSON.parse(localStorage.getItem("todos"));
  }
}
function saveLocalTodos(todo) {
  checkLocalStorage();
  todos.push(todo);
  localStorage.setItem("todos",JSON.stringify(todos));
}
function removeLocalTodos(todo) {
  checkLocalStorage();
  todos.splice(todos.indexOf(todo),1);
  localStorage.setItem("todos",JSON.stringify(todos));
}
function getTodos() {
  checkLocalStorage();
  todos.forEach(function(todo){
    createTodoListItem(todo,"n",);
  });
}
function createTodoListItem(newTodoText,toSaveLocalTodos,) {
  // todo <div>
  const todoDiv = document.createElement("div");
  todoDiv.classList.add("todo");
  // <li>
  const newTodo = document.createElement("li");
  newTodo.innerText = newTodoText;
  newTodo.classList.add("todo-item");
  todoDiv.appendChild(newTodo);
  // add to localStorage
  if (toSaveLocalTodos == "y") {
    saveLocalTodos(todoInput.value);
  }
  // check Button
  const completedButton = document.createElement("button");
  completedButton.innerHTML = '<i class="fa fa-check"></i>';
  completedButton.classList.add("complete-button");
  todoDiv.appendChild(completedButton);
  // trash Button
  const trashButton = document.createElement("button");
  trashButton.innerHTML = '<i class="fa fa-trash"></i>';
  trashButton.classList.add("trash-button");
  todoDiv.appendChild(trashButton);

  // append to todo-list
  todoList.appendChild(todoDiv);
}
